import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import './scss/style.css'
import Home from './views/LogIn/Home';
// import Login from './views/LogIn/LogIn';
import Register from './views/LogIn/Register';

function App() {
  return (
    <HashRouter>
          <Switch>
             <Route path="/register" name="admin" component={Register} />
            <Route path="/" name="Login" component={Home} />
           
          </Switch>
      </HashRouter>
  );
}

export default App;
