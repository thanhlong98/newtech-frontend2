// import React, { Component } from 'react';

// class AccountProfile extends Component {
//     render() {
//         return (
//             <div>
//                 <TabContainer id="left-tabs-example" defaultActiveKey="edit_account">
//                     <Row className='edit-profile-row ' >
//                         <Col md={4} className='edit-profile-col'>
//                             <div>
//                                 <div className='edit-profile'>
//                                     <div className='edit-profile-image'>
//                                         <img className='avatar-image' alt='' src={replaceAvatar} width='65px' height='65px'
//                                             onError={(e) => this.addDefaultSrc(e)}
//                                             onClick={() => this.redirectToProfile()}
//                                             alt='avatar image'
//                                         />
//                                     </div>
//                                     <div className='edit-profile-left'>
//                                         <div className='edit-profile-name'>{profileEdit ? profileEdit.username : ''}</div>
//                                         <div className='edit-profile-title'>
//                                             <div className='edit-profile-upload' onClick={() => this.clickToUpload()}>{common.locale.text_profile_edit_img_upload}</div>
//                                             <input
//                                                 type="file"
//                                                 id="file"
//                                                 ref={this.setUploaderRef}
//                                                 className="d-none"
//                                                 onChange={this.uploadFiles}
//                                             />
//                                         </div>
//                                     </div>
//                                 </div>
//                             </div>
//                             {
//                                 this.customNavLink()
//                             }
//                         </Col>
//                         {
//                             profileEdit &&
//                             <Col md={8} className='edit-profile-col'>
//                                 <TabContent>
//                                     <TabPane eventKey="edit_account">
//                                         <ProfileAccount
//                                             callAgainProfileAPI={this.getProfile}
//                                         />
//                                     </TabPane>
//                                     <TabPane eventKey="edit_password">
//                                         <ProfilePassword />
//                                     </TabPane>
//                                     <TabPane eventKey="edit_application">
//                                         <ProfileLink
//                                             callAgainProfileAPI={this.getProfile}
//                                         />
//                                     </TabPane>
//                                 </TabContent>
//                             </Col>
//                         }
//                     </Row>
//                 </TabContainer>
//             </div>
//         );
//     }
// }

// export default AccountProfile;