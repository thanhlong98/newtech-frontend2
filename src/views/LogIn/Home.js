import React, { Component } from 'react';
import { Tab, Tabs } from "react-bootstrap";
import Login from "../LogIn/LogIn"
import Register from "../LogIn/Register"

class Home extends Component {
    render() {
        return (
            <div className="background-home">
                <div className="home-page">
                    <div className='login-background'>
                    </div>
                    <div className="home-tabs">
                        <Tabs defaultActiveKey='Login' className='tab-container'>
                            <Tab eventKey='Login' title="ĐĂNG NHẬP" tabClassName='login-tab'>
                                <Login/>
                            </Tab>
                            <Tab eventKey='Logout' title="ĐĂNG KÝ" className='login-tab' >
                                <Register/>
                            </Tab>
                        </Tabs>
                    </div>
                </div>

            </div>

        );
    }
}

export default Home;