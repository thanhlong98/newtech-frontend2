import React from 'react';
import { Input } from "reactstrap";
import { UserService } from '../Service/UserService';
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            isSubmitting: false,
            phoneNumber: '',
            error: false
        }
    }

    // handleSubmit = async(event)=>{
    //     // event.preventDefault();
    //     if(this.state.phoneNumber.length ===0 || this.state.password.length===0) {
    //         this.setState({error:true})
    //     }
    //     if(this.state.phoneNumber && this.state.password){
    //         this.setState({isSubmitting:true})
    //         let [success,body]= await UserService.login(this.state.phoneNumber,this.state.password)
    //         if(success && body.errorCode === 200){
    //             this.setState({isSubmitting:false})
    //             let token = body.accessToken
    //             localStorage.setItem('auth-token',token)
    //             window.location.href = "#/admin"
    //         }if(!success){
    //             window.location.reload();
    //         }

    //     }

    // }
    // handleSubmit = () => {
    //     window.location.href = 
    // }
    register = () =>{
        window.location.href = "#/register"
    }
    handlePhoneNumber = (event) => {
        this.setState({ phoneNumber: event.target.value, errorPhoneNumber: false })
    };

    handlePassword = (event) => {
        this.setState({ password: event.target.value, errorPassword: false })
    };
    render() {
        const { error, isSubmitting } = this.state;
        return (
            <div >
                <div className='login-content'>
                    {/* <p className='header-text'>Đăng nhập</p> */}
                    <form onSubmit={this.handleSubmit}>
                        <div className="input-login-content">
                            <Input
                                className='input-login'
                                placeholder='Tên đăng nhập'
                                value={this.state.phoneNumber}
                                onChange={this.handlePhoneNumber}
                            />
                            {error &&
                                <div className='error-text'>Hãy nhập số điện thoại</div>
                            }
                        </div>
                        <div className='input-login-content input-password'>
                            <Input
                                type='password'
                                className='input-login'
                                placeholder='Mật khẩu'
                                value={this.state.password}
                                onChange={this.handlePassword}
                            />
                            {error &&
                                <div className='error-text'>Hãy nhập password</div>
                            }
                        </div>
                        <a className="forget-password" href="#/forget-password">Quên mật khẩu</a><br />
                                <button className='button-submit' disabled={isSubmitting} onClick={this.login}>ĐĂNG NHẬP</button>

                        {/* <div className="login-register">
                            <div className="button-login">
                                <button className='button-submit' disabled={isSubmitting} onClick={this.login}>ĐĂNG NHẬP</button>
                            </div>
                            <div className="button-register">
                                <button className="register-page" onClick={this.register}>ĐĂNG KÝ</button>
                            </div>
                        </div> */}
                    </form>

                </div>
                <div className="footer-ims">
                </div>
            </div>
        )
    }

}
export default Login;